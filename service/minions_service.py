import json

LOCAL_DB_FILE = 'local_db.json'


class MinionsService:

    def __init__(self):
        with open(LOCAL_DB_FILE, 'r') as f:
            self.local_db = json.load(f)

    def get_minion_list(self):
        return self.local_db["minions"]

    def get_minion(self, minion_name):
        for minion in self.local_db["minions"]:
            if minion["name"] == minion_name:
                return minion
        return None
