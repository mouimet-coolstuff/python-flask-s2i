from flask_restplus import Namespace, Resource, fields
from service.minions_service import MinionsService

api = Namespace('minions', description='Minions API')

minion_model = api.model('Minion', {
    'name': fields.String,
    'description': fields.String
})


class MinionViewBase(Resource):

    def __init__(self, api):
        Resource.__init__(self, api)
        self._minions_service = MinionsService()


@api.route("/")
class MinionListView(MinionViewBase):

    @api.marshal_list_with(minion_model, envelope='minions')
    def get(self):
        return self._minions_service.get_minion_list()


@api.route("/<minion_name>")
@api.param('minion_name', 'Minion friendly name')
class MinionView(MinionViewBase):

    @api.marshal_with(minion_model)
    def get(self, minion_name):
        minion = self._minions_service.get_minion(minion_name)

        if minion:
            return minion
        else:
            return api.abort(404, 'Minion %s not found.' % minion_name)

