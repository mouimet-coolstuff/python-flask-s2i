from flask_restplus import Api
from view.minions_view import api as minions_api

api = Api(
    title='Source to Image tester API',
    version='0.1',
    description='Source to Image tester API',
    doc='/apidoc/'
)

api.add_namespace(minions_api)
