# Python Flask S2I Openshift deployment example

This project is an example of a well structured Python app that will easily 
deploy in Openshift using Source to Image (s2i). 

## Key elements in the structure

#### 1 - requirements.txt

The requirements.txt contains the list of all Python packages that are required
by your app. The build engine from Openshift will see that file and will
install all the packages. 

#### 2 - app.py

The main has to be in app.py file at the root directory of your repository. 
This is gone a be the Run command of the Docker image.

#### 3 - Gunicorn integration

wsgi.py

If you have both Django and Gunicorn in your requirements, your Django project
will automatically be served using Gunicorn.

#### 4 - Listen on port 8080

By default, the service associated to the newly created application will
be configured on port 8080 by Openshift.




#### References

https://hub.docker.com/r/centos/python-35-centos7/

